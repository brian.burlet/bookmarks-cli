import { resolve, basename } from 'path';
import { promises as fs, existsSync } from 'fs';
import { homedir, tmpdir } from 'os';
import { prompt } from 'inquirer';

const BM_TMP_FILE = resolve(tmpdir(), 'bookmark.tmp');
const BM_DB_FILE = resolve(homedir(), 'bookmarks.json');

(async () => {
    try {
        await parseArgs(process.argv.slice(2));
    } catch (e) {
        console.error('\x1b[31m%s\x1b[0m', e.message);
        process.exit(1);
    }
})();

async function parseArgs(args: string[]) {
    if (existsSync(BM_TMP_FILE)) {
        await fs.unlink(BM_TMP_FILE);
    }

    const registerIndex = indexOfAny(args, '--register', '-reg');

    if (registerIndex >= 0) {
        args.splice(registerIndex, 1);

        if (args.length > 1) {
            throw new Error('Unexpected number of parameters');
        }

        return registerPathAs(args[0]);
    }

    const removeIndex = indexOfAny(args, '--remove', '-rem');

    if (removeIndex >= 0) {
        args.splice(removeIndex, 1);

        if (args.length > 1) {
            throw new Error('Unexpected number of parameters');
        }

        return removePath(args[0]);
    }

    if (args.length !== 1) {
        throw new Error('Unexpected number of parameters');
    }

    return changeDir(args[0]);
}

async function changeDir(tag: string) {
    const data = await readJson();
    const keys = Object.keys(data).filter(key => {
        return key.includes(tag);
    });

    if (keys.length === 0) {
        throw new Error(`Couldn't find any directory registered under ${tag}`);
    }

    let key = keys[0];

    if (keys.length > 1) {
        const { key } = await prompt([{
            message: 'Select a bookmark:',
            choices: keys.map(key => {
                return {
                    name: `${key}: ${data[key]}`,
                    value: key,
                };
            }),
            name: 'key',
            type: 'list',
        }]);
    }

    await fs.writeFile(BM_TMP_FILE, data[key], 'utf8');
}

async function removePath(tag?: string) {
    const data = await readJson();

    if (tag) {
        if (!data[tag]) {
            throw new Error(`tag ${tag} wasn't found, so couldn't be removed`);
        }

        delete data[tag];
    } else {
        let found = false;
        const keys = Object.keys(data);
        const curPath = resolve();

        for (const key of keys) {
            if (data[key] === curPath) {
                found = true;
                delete data[key];
            }
        }

        if (!found) {
            throw new Error(`didn't find any tags for which ${curPath} was registered`);
        }
    }

    await storeJson(data);
}

async function registerPathAs(tag?: string) {
    const curPath = resolve();
    tag = tag || basename(curPath);

    const data = await readJson();

    data[tag] = curPath;

    await storeJson(data);
}

function indexOfAny<T>(array: T[], ...queries: T[]) {
    for (const query of queries) {
        const i = array.indexOf(query);

        if (i >= 0) {
            return i;
        }
    }

    return -1;
}

async function storeJson(data: Record<string, string>) {
    await fs.writeFile(BM_DB_FILE, JSON.stringify(data, null, 4), 'utf8');
}
async function readJson(): Promise<Record<string, string>> {
    try {
        return JSON.parse(await fs.readFile(BM_DB_FILE, 'utf8'));
    } catch (e) {
        return {};
    }
}
