# Installation
Add the below code to the `.bashrc`-file.

```bash
function bm() {
  local BM_FILE="/tmp/bookmark.tmp"

  node ~/Documents/bookmarks/dist/bookmarks.js $@

  if [[ -f "$BM_FILE" ]]; then
    local BOOKMARK_DIR="$(cat $BM_FILE)"

    if [[ -n "$BOOKMARK_DIR" ]]; then
      cd "$BOOKMARK_DIR"
    fi
  fi
}
```
